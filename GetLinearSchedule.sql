-- GET Linear Schedule
-- Given: startDate, endDate, timezone, networkFeedCode [values depend on network - TBS, TNT, TCM etc]
select label, external_id, date_format(str_to_date(convert_tz(cast(start_date as datetime), '+00:00', '-04:00'), '%Y-%m-%e %T'), '%Y-%m-%e %r'), date_format(str_to_date(convert_tz(cast(end_date as datetime), '+00:00', '-04:00'), '%Y-%m-%e %T'), '%Y-%m-%e %r'), franchise_duration, franchise_name, content_json
from content_linear_schedule
where cast(start_date as datetime) >= str_to_date(convert_tz(str_to_date('2016-10-31 00:00:00', '%Y-%m-%e %T'), '-04:00', '+00:00'), '%Y-%m-%e %T')
and cast(start_date as datetime) < str_to_date(convert_tz(str_to_date('2016-11-01 00:00:00', '%Y-%m-%e %T'), '-04:00', '+00:00'), '%Y-%m-%e %T')
and network_feed_code = 'E'
order by cast(start_date as datetime);